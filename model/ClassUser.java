package com.example.bict_register.model;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity(name = "classUser")
public class ClassUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, length = 128)
    @NotEmpty
    @NotNull
    private String username;

    @Column(nullable = false, length = 128)
    @NotEmpty
    @NotNull
    private String password;

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
