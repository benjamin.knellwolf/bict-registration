package com.example.bict_register.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Date;

@Entity(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, length = 32)
    @NotEmpty
    @NotNull
    private String gender;

    @Column(nullable = false, length = 64)
    @NotEmpty
    @NotNull
    private String firstName;

    @Column(nullable = false, length = 64)
    @NotEmpty
    @NotNull
    private String lastName;

    @Column(nullable = false, length = 64)
    @NotEmpty
    @NotNull
    private String ahvNumber;

    @Column(nullable = false)
    @NotEmpty
    @NotNull
    private Date birthDate;

    @Column(nullable = false)
    @NotEmpty
    @NotNull
    private String street;

    @Column(nullable = false)
    @NotEmpty
    @NotNull

    private int streetNumber;
    @Column(nullable = false)
    @NotEmpty
    @NotNull
    private String location;

    @Column(nullable = false)
    @NotEmpty
    @NotNull
    private int plz;

    @Column(nullable = false, length = 128)
    @Email
    @NotEmpty
    @NotNull
    private String personalEmail;

    @Column(nullable = false, length = 128)
    @NotEmpty
    @NotNull
    private String personalPhoneNumber;

    @Column(nullable = false, length = 128)
    @NotEmpty
    @NotNull
    private String personalLandLine;

    @Column(nullable = false, length = 128)
    @NotEmpty
    @NotNull
    private String iBan;

    @Column(nullable = false, length = 128)
    @NotEmpty
    @NotNull
    private int parentPhoneNumber;

    @Column(nullable = false, length = 128)
    @Email
    @NotEmpty
    @NotNull
    private String parentEmail;

    @Column(nullable = false, length = 64)
    @NotEmpty
    @NotNull
    private String schoolClass;

    public Long getId() {
        return id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAhvNumber() {
        return ahvNumber;
    }

    public void setAhvNumber(String ahvNumber) {
        this.ahvNumber = ahvNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public String getPersonalPhoneNumber() {
        return personalPhoneNumber;
    }

    public void setPersonalPhoneNumber(String personalPhoneNumber) {
        this.personalPhoneNumber = personalPhoneNumber;
    }

    public String getPersonalLandLine() {
        return personalLandLine;
    }

    public void setPersonalLandLine(String personalLandLine) {
        this.personalLandLine = personalLandLine;
    }

    public String getiBan() {
        return iBan;
    }

    public void setiBan(String iBan) {
        this.iBan = iBan;
    }

    public int getParentPhoneNumber() {
        return parentPhoneNumber;
    }

    public void setParentPhoneNumber(int parentPhoneNumber) {
        this.parentPhoneNumber = parentPhoneNumber;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }
}
