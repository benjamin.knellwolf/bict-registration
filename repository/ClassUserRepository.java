package com.example.bict_register.repository;

import com.example.bict_register.model.ClassUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassUserRepository extends JpaRepository<ClassUser, Long>, CrudRepository<ClassUser, Long> {

    @Query("SELECT cu FROM classUser cu WHERE cu.username = :username AND cu.password = :password")
    ClassUser findClassUserByUsernameAndAndPassword(@Param("username") String username, @Param("password") String password);
}
