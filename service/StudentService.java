package com.example.bict_register.service;

import com.example.bict_register.model.Student;
import com.example.bict_register.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public Student saveStudent(Student student) {
        return this.studentRepository.saveAndFlush(student);
    }

    public List<Student> findAllStudents() {
        return this.studentRepository.findAll();
    }

    public void deleteStudent(Long userId) {
        this.studentRepository.deleteById(userId);
    }
}
