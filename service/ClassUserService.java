package com.example.bict_register.service;

import com.example.bict_register.model.ClassUser;
import com.example.bict_register.repository.ClassUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassUserService {

    @Autowired
    private ClassUserRepository classUserRepository;

    public ClassUser saveClassUser(ClassUser classUser) {
        return this.classUserRepository.saveAndFlush(classUser);
    }

    public ClassUser loginClassUser(ClassUser classUser) {
        return this.classUserRepository.findClassUserByUsernameAndAndPassword(classUser.getUsername(), classUser.getPassword());
    }
}
