package com.example.bict_register.controller;

import com.example.bict_register.model.Student;
import com.example.bict_register.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping
    public Student registerStudent(@RequestBody Student student) {
        return this.studentService.saveStudent(student);
    }

    @GetMapping
    public List<Student> getStudents() {
        return this.studentService.findAllStudents();
    }

    @DeleteMapping(value = "/{userId}")
    public void deleteUser(@PathVariable Long userId) {
        this.studentService.deleteStudent(userId);
    }
}
