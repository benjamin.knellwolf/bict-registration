package com.example.bict_register.controller;

import com.example.bict_register.model.ClassUser;
import com.example.bict_register.service.ClassUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/classUser")
public class ClassUserController {

    @Autowired
    private ClassUserService classUserService;

    @RequestMapping(value = "/")
    public ClassUser loginClassUser(@RequestBody ClassUser classUser) {
        return this.classUserService.loginClassUser(classUser);
    }
}
